<?php

Class users{

	public $id;
	public $username;
	public $email;

	public static function findAll(){
		$query = "SELECT * FROM users";
		$database = new Database();
		$result = $database->query($query);
		$temporary = array();
		while($line = mysqli_fetch_array($result)){
			$users = new users();
			$users->id = $line["id"];
			$users->username = $line["username"];
			$users->email = $line["email"];
			array_push($temporary, $users);
		}
		return $temporary;
	}	
}

?>