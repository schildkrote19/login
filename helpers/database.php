<?php

Class database {

	private $connection;

	public function __construct(){
		$this->connection = mysqli_connect("localhost", "root", "", "login");
	}

	public function query($query){
		return mysqli_query($this->connection,$query);
	}

	public function __destruct(){
		mysqli_close($this->connection);
	}
}

?>