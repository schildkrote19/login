CREATE TABLE `users`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(50) NOT NULL DEFAULT '',
    `password` VARCHAR(128) NOT NULL DEFAULT '',
    `email` VARCHAR(250) NOT NULL DEFAULT '',
    `active` BINARY(1) NOT NULL DEFAULT '0',
    PRIMARY KEY(`id`)
)