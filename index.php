<html>
<head>
	<title>Home Page</title>
</head>
<body>
<?php
session_start();
session_destroy();

function __autoload($class){
	$folders = array("controllers","models","helpers");
	foreach($folders as $folder){
		$path="$folder/$class.php";
		if(file_exists($path)){
			require $path;
		}
	}
}

?>
<a href="index.php?C=register&A=users"> Register |</a>
<a href="index.php?C=login&A=users"> Log in </a>
<div>
<?php

if(array_key_exists("C", $_GET)){

	$controller = $_GET["C"]."controller";
} else {

	$controller = "";
}

if(class_exists($controller))
{
    $object = new $controller();

    if(array_key_exists("A", $_GET)){

        $action = $_GET["A"]."action";
    } else {

        $action = "";
    }
    if(method_exists($object, $action)){

        $object->$action();
    } else {

        die("method not found.");
    }
}

?>
</div>
</body>
</html>